package com.heaven.sky.common.encrypt;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public final class NMessageDigests {

    private NMessageDigests() { }

    public static MessageDigest getAlgorithm(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalArgumentException("has no algorithm: " + algorithm);
        }
    }

}
