package com.heaven.sky.common.encrypt;

import com.heaven.sky.common.utils.StringUtil;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;

public final class NSha256 {

    private static final String ALGORITHM = "SHA-256";

    private NSha256() {
    }

    public static String cryptUtf(String originalString) {
        byte[] encodedhash = cryptUtfToBytes(originalString);
        String hex = StringUtil.printHex(encodedhash);
        return hex;
    }

    public static byte[] cryptUtfToBytes(String originalString) {
        MessageDigest digest = NMessageDigests.getAlgorithm(ALGORITHM);
        byte[] bytes = originalString.getBytes(StandardCharsets.UTF_8);
        byte[] encodedhash = digest.digest(bytes);
        return encodedhash;
    }

}
