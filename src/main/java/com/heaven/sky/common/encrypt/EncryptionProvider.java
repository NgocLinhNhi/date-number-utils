package com.heaven.sky.common.encrypt;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;

public class EncryptionProvider {
    private static EncryptionProvider instance = null;

    public EncryptionProvider(KeyPair keypair) throws NoSuchAlgorithmException,
            NoSuchProviderException {
        KeyPairGenerator keygenerator = KeyPairGenerator.getInstance("RSA");
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG", "SUN");
        keygenerator.initialize(1024, random);
        keypair = keygenerator.generateKeyPair();
        // how can i call constructor to get keypair; if i call does this gonna
        // work
    }

    public static EncryptionProvider getInstance() {
        if (instance == null) {
            instance = new EncryptionProvider();
        }
        return instance;
    }

    public EncryptionProvider() {
        //  Auto-generated constructor stub
    }

    public String ENCRYPT(String Algorithm, String Data) throws Exception {
        String alg = Algorithm;
        String data = Data;
        byte[] encrypted = null;
        if (alg.equals("AES")) {
            Key key = generateKey();
            Cipher cipher;
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, key);
            encrypted = cipher.doFinal(data.getBytes());
        } else if (alg.equals("RSA")) {
            KeyPair keypair = new KeyPair(null, null); // initiaization problem
            new EncryptionProvider(keypair); // does this works??
            PublicKey publicKey = keypair.getPublic();
            Cipher cipher;
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            encrypted = cipher.doFinal(data.getBytes());
        }

        return asHex(encrypted);
    }

    public String DECRYPT(String Algorithm, String data) throws Exception {
        String alg = Algorithm;
        String Decrypted = "";
        byte[] input = hexStringToByteArray(data);
        if (alg.equals("AES")) {
            Key key = generateKey();
            Cipher cipher;
            cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.DECRYPT_MODE, key);
            byte[] original = cipher.doFinal(input);
            Decrypted = new String(original);
        } else if (alg.equals("RSA")) {

            KeyPair keypair = new KeyPair(null, null);
            new EncryptionProvider(keypair);
            PrivateKey privateKey = keypair.getPrivate();
            Cipher cipher;
            cipher = Cipher.getInstance("RSA");
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            byte[] dec = cipher.doFinal(input);
            Decrypted = new String(dec);

        }
        return Decrypted;
    }

    private static Key generateKey() throws Exception {
        byte[] keyValue = new byte[]{'T', 'h', 'i', 's', 'I', 's', 'A', 'S',
                'e', 'c', 'r', 'e', 't', 'K', 'e', 'y'};
        Key key = new SecretKeySpec(keyValue, "AES");
        return key;
    }

    public static String asHex(byte buf[]) {
        StringBuffer strbuf = new StringBuffer(buf.length * 2);
        int i;
        for (i = 0; i < buf.length; i++) {
            if (((int) buf[i] & 0xff) < 0x10)
                strbuf.append("0");
            strbuf.append(Long.toString((int) buf[i] & 0xff, 16));
        }

        return strbuf.toString();
    }

    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

}
