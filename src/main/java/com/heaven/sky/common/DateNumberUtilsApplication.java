package com.heaven.sky.common;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DateNumberUtilsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DateNumberUtilsApplication.class, args);
    }

}
