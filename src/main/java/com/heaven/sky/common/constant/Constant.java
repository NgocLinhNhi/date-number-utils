package com.heaven.sky.common.constant;

public class Constant {
    public static final String PATTERN_YYMMDD = "yyyy/MM/dd";
    public static final String PATTERN_YYMMDD_HH_MM = "yyyy/MM/dd HH:mm";
    public static final String PATTERN_YYMMDD_BLANK = "yyyyMMdd";
    public static final String PATTERN_YYYYMd = "yyyy/M/d";
    public static final String PATTERN_YYYMMDD_HHMMSS_FULL_SLASH = "yyyy/MM/dd HH:mm:ss";
    public static final String PATTERN_SLASH_YYYYMMDD_HH_COLON_MM = "yyyy/MM/dd HH:mm";
    public static final String PATTERN_SLASH_YYYYMMDD_HH_COLON_MM_SS = "yyyy/MM/dd HH:mm:ss";
    public static final String PATTERN_FOR_GEN_ID = "MMddHHmm";
    public static final String PATTERN_YYYMMDD_HHMMSS = "yyyyMMdd HH:mm:ss";
    public static final String PATTERN_YYYMMDD_HHMMSS_FULL = "yyyy-MM-dd HH:mm:ss";
    public static final String PATTERN_YYMMDD_HHMMSS_BLANK = "yyyyMMddHHmmss";
    public static final String PATTERN_YYYYMM_BLANK = "yyyyMM";
    public static final String PATTERN_YYYYMM_DASH = "yyyy-MM";
    public static final String PATTERN_YYYYMM_SEPARATOR = "yyyy/MM";
    public static final String PATTERN_YYYY = "yyyy";
    public static final String PATTERN_HH_COLON_MM = "HH:mm";
    public static final String PATTERN_HHMMSS_BLANK = "HHmmss";
    public static final String PATTERN_YYMMDD_HHMMSSS = "yyyy-MM-dd HH:mm:ss.S";
    public static final String PATTERN_YYYMMDD_HHMMSS_SEMI_SSS = "yyyyMMdd HH:mm:ss:SSS";
    public static final String FORMAT_USER_DATETIME = "yyyy-MM-dd hh:mm:ss.SSS";
    public static final String PATTERN_YYYYMMDD_HHMMSS = "yyyyMMdd_HHmmss";
    public static final String PATTERN_YYYYMMDD_HH = "yyyyMMddHH";
    public static final String PATTERN_HHmmSSS = "HH:mm:ss.SSS";



    public static class ROLES_ID {
        public static final int ROLE_ADMIN = 1;
        public static final int ROLE_CREATER = 2;
        public static final int ROLE_APPROVER = 3;
    }


}
