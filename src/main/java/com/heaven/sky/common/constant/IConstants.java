package com.heaven.sky.common.constant;

public interface IConstants {

    String UTF8 = "UTF-8";
    String DEFAULT_FIRST_DATE_OF_YEAR = "0101";

    interface MAIL_SEND_FLG {
        int ALLOW = 1;
    }


    interface CURRENCY {
        Integer USD = 1;
        Integer JPY = 6;
    }

    interface AUTHEN {
        int NONE = 0;
        int READ = 1;
        int WRITE = 2;
        int IGNORED = -1;
    }

    interface SYS_PROPERTY {
        String STATUS = "Status";
        String SERVICE_TYPE = "ServiceType";
        String WITHDRAWAL_METHOD = "WithdrawalMethod";
        String FRONT_END = "FrontEnd";
        String MAIL_TYPE = "MailType";
        String LANGUAGE = "Language";

        String MAIL_TEMPLATE_PARAM = "MailTemplateParam";
        String ORDER_TYPE = "ORDER_TYPE";
        String ORDER_STATUS = "ORDER_STATUS";

        String BJP = "BJP";

        String BASE_CURRENCY = "BASE_CURRENCY";
    }


    interface SORT_TYPE {
        Integer FROM_NEWEST = 1;
        Integer CUSTOMER_ID = 3;
    }

    interface BO_CUSTOMER_CONFIG {
        String BOWEB_TUTORIAL_FLG = "BOWEB_TUTORIAL_FLG";
        String ONE_CLICK_ORDER = "ONE_CLICK_ORDER";
        String SEC_AUTO_LOGOUT = "SEC_AUTO_LOGOUT";
        String SEC_AUTO_LOGOUT_TIME = "SEC_AUTO_LOGOUT_TIME";
        String SEND_MAIN_MAIL_FLG = "SEND_MAIN_MAIL_FLG";
        String SEND_ADDITION_MAIL_FLG = "SEND_ADDITION_MAIL_FLG";
        String CUSTOMER_SETTING_TRUE = "1";
        String CUSTOMER_SETTING_FALSE = "0";
        String AUTO_LOGOUT_MINUTES_DEFAULT = "60";
    }

}
