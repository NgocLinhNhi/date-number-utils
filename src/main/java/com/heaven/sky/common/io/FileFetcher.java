package com.heaven.sky.common.io;

import java.io.File;

public interface FileFetcher {

    File getFile(String filePath);

}
