package com.heaven.sky.common.io;

import java.io.InputStream;

public interface InputStreamFetcher {

    InputStream getInputStream(String filePath);

}
