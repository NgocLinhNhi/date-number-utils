package com.heaven.sky.common.io;

import java.io.FileInputStream;
import java.io.InputStream;

public class AnywayInputStreamFetcher implements InputStreamFetcher {

    private static final String SEPARATOR = "/";

    @Override
    public InputStream getInputStream(String filePath) {
        InputStream inputStream = getClass().getResourceAsStream(filePath);
        if (inputStream == null)
            inputStream = getClass().getResourceAsStream(SEPARATOR + filePath);
        if (inputStream == null) {
            try {
                inputStream = new FileInputStream(filePath);
            } catch (Exception e) {
                throw new IllegalArgumentException("has no input stream to file: " + filePath, e);
            }
        }
        return inputStream;
    }
}
