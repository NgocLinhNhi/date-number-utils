package com.heaven.sky.common.utils.cipher;

import java.util.*;

public class StringExamTestUtil {

    private static String[] inputs = {"float", "flin", "flop"};
    private static final List<String> listString = new ArrayList<>();

    public static void main(String[] args) {
        String output = longestCommonPrefix(inputs);
        System.out.println(output);
    }

    public static String longestCommonPrefix(String[] strs) {
        if (strs == null || strs.length == 0) {
            return "";
        }
        Arrays.sort(strs);
        String prefix = strs[0];
        for (int i = 1; i < strs.length; i++) {
            while (!strs[i].startsWith(prefix)) {
                prefix = prefix.substring(0, prefix.length() - 1);
                if (prefix.isEmpty()) {
                    return "";
                }
            }
        }
        return prefix;
    }

}
