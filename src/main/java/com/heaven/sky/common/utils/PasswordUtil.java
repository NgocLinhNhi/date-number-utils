package com.heaven.sky.common.utils;

import org.apache.commons.lang3.StringUtils;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class PasswordUtil {

	private static final String RANDOM_STR = "0123456789abcdefghijklmnopqrstuvwxyz";
	private static final String RANDOM_NUMBER = "0123456789";

	public static boolean isNumeric(String number) {
		boolean isValid = false;
		String expression = "^[-+]?[0-9]*\\.?[0-9]+$";
		CharSequence inputStr = number;
		Pattern pattern = Pattern.compile(expression);
		Matcher matcher = pattern.matcher(inputStr);
		if (matcher.matches()) {
			isValid = true;
		}
		return isValid;
	}

	public static String generateRandomPassword(int len) {
		Random rand = new Random();
		StringBuffer sb = new StringBuffer(len);
		char str;
		boolean checkOneNumber = false;
		for (int i = 0; i < len; i++) {
			str = RANDOM_STR.charAt(rand.nextInt(RANDOM_STR.length()));
			if (StringUtils.isNumeric(str + "")) {
				checkOneNumber = true;
			}
			sb.append(str);
		}
		
		// set one up case character
		int position1 = rand.nextInt(len);
		char str_check = sb.charAt(position1);
		while (StringUtils.isNumeric(str_check + "")) {
			position1 = rand.nextInt(len);
			str_check = sb.charAt(position1);
		}
		
		sb.setCharAt(position1, Character.toUpperCase(str_check));

		// check at least one number
		if (!checkOneNumber) {
			int position3 = rand.nextInt(len);
			while (position3 == position1) {
				position3 = rand.nextInt(len);
			}
			sb.setCharAt(position3,
					RANDOM_NUMBER.charAt(rand.nextInt(RANDOM_NUMBER.length())));
		}

		return sb.toString();
	}

	public static char toLower (char c) {
		return c |= 32;
	}
	
	public static char toUpper(char c) {
		return c &= ~32;
	}

	public static boolean isEmpty(String str) {
		if (str == null || str.length() <= 0)
			return true;
		return false;
	}

}
