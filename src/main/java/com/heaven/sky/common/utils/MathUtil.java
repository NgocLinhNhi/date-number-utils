package com.heaven.sky.common.utils;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.text.DecimalFormat;


public class MathUtil {

    private static Logger logger = LoggerFactory.getLogger(MathUtil.class);

    /**
     *
     * Parse String to Integer
     *
     * @param str
     * @return
     */
    public static Integer parseInteger(String str) {
        return parseInteger(str, null);
    }

    public static Integer parseInteger(String str, Integer defValue) {
        if (StringUtil.isEmpty(str))
            return defValue;
        try {
            return Integer.parseInt(str.trim());
        } catch (Exception e) {
            logger.warn("can't parse int from string: {}", str);
            logger.warn("error: ", e);
            return defValue;
        }
    }

    /**
     *
     * Parse String to Long
     *
     * @param str
     * @return
     */
    public static Long parseLong(String str) {
        return parseLong(str, null);
    }

    public static Long parseLong(String str, Long defValue) {
        if (StringUtil.isEmpty(str))
            return defValue;
        try {
            return Long.parseLong(str.trim());
        } catch (Exception e) {
            logger.warn("can't parse long from string: {}", str);
            logger.warn("error: ", e);
            return defValue;
        }
    }

    /**
     *
     * Parse String to double
     *
     * @param str
     * @return
     */
    public static Double parseDouble(String str) {
        return parseDouble(str, null);
    }

    public static Double parseDouble(String str, Double defValue) {
        if (StringUtil.isEmpty(str))
            return defValue;
        StringBuffer bf = new StringBuffer(str);
        int index = bf.indexOf(",");
        while (index != -1) {
            bf.deleteCharAt(index);
            index = bf.indexOf(",");
        }
        try {
            return Double.parseDouble(bf.toString().trim());
        } catch (Exception e) {
            logger.warn("can't parse double from string: {}", str);
            logger.warn("error: ", e);
            return defValue;
        }
    }

    /**
     *
     * Parse String to Int
     *
     * @param str
     * @return
     */
    public static int parseInt(String str) {
        return parseInt(str, 0);
    }

    public static int parseInt(String str, int defValue) {
        if (StringUtil.isEmpty(str))
            return defValue;
        try {
            return Integer.parseInt(str.trim());
        } catch (Exception e) {
            logger.warn("can't parse int from string: {}", str);
            logger.warn("error: ", e);
            return defValue;
        }
    }

    /**
     *
     * Parse String to long
     *
     * @param str
     * @param defValue
     * @return
     */
    public static long parseLong(String str, long defValue) {
        if (StringUtil.isEmpty(str))
            return defValue;
        try {
            return Long.parseLong(str.trim());
        } catch (Exception e) {
            logger.warn("can't parse long from string: {}", str);
            logger.warn("error: ", e);
            return defValue;
        }
    }

    /**
     *
     * Parse Object to BigDecimal
     *
     * @param obj
     * @return
     */
    public static BigDecimal parseBigDecimal(Object obj) {
        if (obj == null)
            return new BigDecimal("0");
        String str = obj.toString().trim();
        if (StringUtil.isEmpty(str))
            return new BigDecimal("0");
        try {
            StringBuffer bf = new StringBuffer(str);
            int index = bf.indexOf(",");
            while (index != -1) {
                bf.deleteCharAt(index);
                index = bf.indexOf(",");
            }
            return new BigDecimal(bf.toString());
        } catch (Exception e) {
            logger.warn("can't parse BigDecimal from string: {}", str);
            logger.warn("error: ", e);
            return new BigDecimal("0");
        }
    }

    public static String formatNumber(Number number,
                                      int decimalPlace,
                                      int roundingMode,
                                      boolean isFixedDecimalPlace, boolean isSeparateThousand) {
        //Start validate params
        if (number == null) {
            number = new Double(0);
        }
        if (decimalPlace < 0) {
            decimalPlace = 0;
        }
        if (roundingMode != BigDecimal.ROUND_CEILING || roundingMode != BigDecimal.ROUND_DOWN ||
                roundingMode != BigDecimal.ROUND_FLOOR || roundingMode != BigDecimal.ROUND_HALF_DOWN ||
                roundingMode != BigDecimal.ROUND_HALF_EVEN || roundingMode != BigDecimal.ROUND_HALF_UP ||
                roundingMode != BigDecimal.ROUND_UNNECESSARY || roundingMode != BigDecimal.ROUND_UP) {
            roundingMode = BigDecimal.ROUND_HALF_UP;
        }
        //End validate params

        BigDecimal bd = new BigDecimal(number.toString());
        bd = bd.setScale(decimalPlace, roundingMode);
        String format = (isSeparateThousand ? "#," : "") + "##0";
        String afterDecimalPlace = StringUtils.repeat(isFixedDecimalPlace ? '0' : '#', decimalPlace);
        format = StringUtil.isEmpty(afterDecimalPlace) ? format : format + "." + afterDecimalPlace;
        DecimalFormat df = new DecimalFormat(format);
        return df.format(bd);
    }

    /**
     *
     * parse BigDecimal to double
     *
     * @param c
     * @return
     */
    public static double doubleValue(BigDecimal c) {
        if (c == null)
            return 0;
        return c.doubleValue();
    }
}
