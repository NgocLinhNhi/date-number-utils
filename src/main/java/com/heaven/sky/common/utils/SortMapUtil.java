package com.heaven.sky.common.utils;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SortMapUtil {

    //Desc
    public static LinkedHashMap<Integer, Integer> sortHashMapDesc(Map<Integer, Integer> unSortedMap) {
        LinkedHashMap<Integer, Integer> reverseSortedMap = new LinkedHashMap<>();

        unSortedMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .forEachOrdered(x -> reverseSortedMap.put(x.getKey(), x.getValue()));
        return reverseSortedMap;
    }

    //Asc
    public static LinkedHashMap<Integer, Integer> sortHashMapAsc(Map<Integer, Integer> unSortedMap) {
        LinkedHashMap<Integer, Integer> SortedMap = new LinkedHashMap<>();

        unSortedMap.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue())
                .forEachOrdered(x -> SortedMap.put(x.getKey(), x.getValue()));
        return SortedMap;
    }
}
