/**
 *
 */
package com.heaven.sky.common.utils;

import java.util.Comparator;
import java.util.Map;

public class ValueComparator implements Comparator<Integer> {

    Map<Integer, String> base;

    public ValueComparator(Map<Integer, String> base) {
        this.base = base;
    }

    @Override
    public int compare(Integer o1, Integer o2) {
        if (o1 == -1)
            return -1;
        if (o2 == -1)
            return 1;
        String sa = base.get(o1);
        String sb = base.get(o2);
        return sa.compareTo(sb);
    }


}
