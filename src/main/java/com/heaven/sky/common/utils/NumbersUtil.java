package com.heaven.sky.common.utils;

import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Random;

public final class NumbersUtil {

    private NumbersUtil() {
    }

    /**
     * Round for number all type
     *
     * @param number
     * @param scale
     * @param round
     * @return
     */
    public static BigDecimal format(BigDecimal number, int scale, int round) {
        BigDecimal result = null;
        int roundMode = getRoundMode(round);
        if (number == null) {
            return result;
        }
        try {
            return number.setScale(scale, roundMode);
        } catch (NumberFormatException e) {
        }
        return result;
    }

    /**
     * Round with double
     *
     * @param number
     * @param scale
     * @param round
     * @return
     */
    public static Double formatBigDecimalToDouble(BigDecimal number, int scale, int round) {
        Double result = null;
        int roundMode = getRoundMode(round);
        if (number == null) {
            return result;
        }
        try {
            return number.setScale(scale, roundMode).doubleValue();
        } catch (NumberFormatException e) {
        }
        return result;
    }

    private static int getRoundMode(int round) {
        switch (round) {
            case 0:
                round = BigDecimal.ROUND_UP;
                break;
            case 1:
                round = BigDecimal.ROUND_DOWN;
                break;
            case 2:
                round = BigDecimal.ROUND_CEILING;
                break;
            case 3:
                round = BigDecimal.ROUND_FLOOR;
                break;
            case 4:
                round = BigDecimal.ROUND_HALF_UP;
                break;
            case 5:
                round = BigDecimal.ROUND_HALF_DOWN;
                break;
            case 6:
                round = BigDecimal.ROUND_HALF_EVEN;
                break;
            case 7:
                round = BigDecimal.ROUND_UNNECESSARY;
                break;
            default:
                round = BigDecimal.ROUND_UP;
                break;
        }
        return round;
    }


    public static String formatAmount(BigDecimal amount, int scale, int roundMode) {
        if (amount == null) return "";
        amount = format(amount, scale, roundMode);
        StringBuilder formatPattern = new StringBuilder("###,###");
        for (int i = 0; i < scale; i++) {
            if (i == 0) formatPattern.append(".");
            formatPattern.append("#");
        }
        return new DecimalFormat(formatPattern.toString()).format(amount);
    }

    /**
     * Format percent
     *
     * @param number
     * @param scale
     * @param mode
     * @param minimumFractionDigits
     * @param maximumFractionDigits
     * @return
     */
    public static String formatAmountPercent(
            BigDecimal number,
            int scale,
            RoundingMode mode,
            int minimumFractionDigits,
            int maximumFractionDigits) {

        if (number == null) return "";
        number = number.setScale(scale, mode);
        DecimalFormat df = new DecimalFormat("#,###.##%");
        df.setMaximumFractionDigits(maximumFractionDigits);
        df.setMinimumFractionDigits(minimumFractionDigits);

        String result = df.format(number);
        return result;
    }

    /**
     * Check is Integer Number
     *
     * @param bd
     * @return
     */
    public static boolean isIntegerValue(BigDecimal bd) {
        return bd.stripTrailingZeros().scale() <= 0;
    }

    /**
     * Check money > 0
     *
     * @param money
     * @return
     */
    public static boolean isPositive(BigDecimal money) {
        if (money == null) return false;
        return money.compareTo(BigDecimal.ZERO) > 0;
    }

    /**
     * Add money
     *
     * @param money1
     * @param money2
     * @return
     */
    public static BigDecimal add(BigDecimal money1, BigDecimal money2) {
        if (money1 == null && money2 == null) return null;
        if (money2 == null) return money1;
        return money1 == null ? money2 : money1.add(money2);
    }

    /**
     * Sum total money
     *
     * @param values
     * @return
     */
    public static BigDecimal sum(final Collection<BigDecimal> values) {
        if (CollectionUtils.isEmpty(values)) return BigDecimal.ZERO;
        BigDecimal money = BigDecimal.ZERO;
        for (BigDecimal v : values) {
            if (v != null) money = money.add(v);
        }
        return money;
    }

    public static Short safeToShort(Object obj1) throws Exception {
        Short result = Short.valueOf((short) 0);
        if (obj1 != null) result = Short.parseShort(obj1.toString());
        return result;
    }

    public static int safeToInt(Object obj1) throws Exception {
        int result;
        if (obj1 == null) {
            return 0;
        } else {
            result = Integer.parseInt(obj1.toString());
            return result;
        }
    }

    public static Long safeToLong(Object obj1) throws Exception {
        Long result = 0L;
        if (obj1 != null) result = Long.parseLong(obj1.toString());
        return result;
    }

    public static Double safeToDouble(Object obj1) throws Exception {
        Double result = 0.0D;
        if (obj1 != null) result = Double.parseDouble(obj1.toString());
        return result;
    }

    public static BigDecimal safeToBigDecimal(Object obj1) throws Exception {
        BigDecimal result = new BigDecimal(0);
        if (obj1 == null) {
            return result;
        } else {
            result = new BigDecimal(obj1.toString());
            return result;
        }
    }

    public static String safeToString(Object obj1) {
        return obj1 == null ? "" : obj1.toString();
    }

    public static boolean safeEqual(Long obj1, Long obj2) {
        if (obj1 == null && obj2 == null) {
            return true;
        } else {
            return obj1 != null && obj2 != null && obj1.compareTo(obj2) == 0;
        }
    }

    public static boolean checkLongNumber(BigDecimal minCar) {
        try {
            Long.parseLong(minCar.toString());
            return true;
        } catch (Exception var2) {
            return false;
        }
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt(max - min + 1) + min;
        return randomNum;
    }

    public static Long convertDoubleToLong(Double value) {
        return value == null ? 0L : value.longValue();
    }

    public static Double divide(String ts, String ms) {
        return ms.equals("0") ? 0.0D : Double.parseDouble(ts) / Double.parseDouble(ms);
    }

}
