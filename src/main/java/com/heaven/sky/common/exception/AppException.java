package com.heaven.sky.common.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AppException extends Exception {

    private static final long serialVersionUID = 1849153155555456859L;

    private String errorCode;
    private String errorMessage;

    public AppException(String errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

}
