package com.heaven.sky.common.load.property;

import com.heaven.sky.common.io.InputStreams;
import lombok.Getter;

import java.io.IOException;
import java.util.Properties;

public class AppConfig {
    //bình thường project SpringBoot run file application.run thì sẽ tự lấy đc value trong file application.properties dễ dàng = @Value
    //Tuy nhiên chạy run hàm main bt = test sẽ không lấy được value ( dùng khi unit test đâu -> nên lấy value trong file application.properties kiểu này

    public static final String APPLICATION_PROPERTIES = "application.properties";
    public static final String CUSTOMER_URI_POJO = "get.customer.url.pojo";

    private static AppConfig instance;

    @Getter
    private static String customerUriPOJO;

    public static synchronized AppConfig getInstance() {
        if (instance == null) {
            instance = new AppConfig();
        }
        return instance;
    }

    public void loadProperties() throws IOException {
        Properties properties = new Properties();
        properties.load(InputStreams.getInputStream(APPLICATION_PROPERTIES));

        customerUriPOJO = properties.getProperty(CUSTOMER_URI_POJO);
    }

    public static void main(String[] args) throws IOException {
        AppConfig.getInstance().loadProperties();
        System.out.println(customerUriPOJO);
    }
}
