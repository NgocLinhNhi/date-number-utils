package com.heaven.sky.common.decrypt;


import org.apache.commons.codec.binary.Base64;
import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

import java.io.*;
import java.text.ParseException;

public class Decrypt {
    String SMTP_AUTH_USER = "7038e6060ae9ff5ff711bd1cfcab2429";
    String PASS = "3fa3154e07c61bfca8ec66b1958a9494";

    public Decrypt() {
    }

    public static void main(String[] args) throws ParseException {
        String decrypt = encryptData("HKVT");
        System.err.println(decrypt);
        String decrypt1 = encryptData("HKVT147@$^");
        System.err.println(decrypt1);
    }

    private static void encode(String sourceFile, String targetFile, boolean isChunked) throws Exception {
        byte[] base64EncodedData = Base64.encodeBase64(loadFileAsBytesArray(sourceFile), isChunked);
        writeByteArraysToFile(targetFile, base64EncodedData);
    }

    public static void decode(String sourceFile, String targetFile) throws Exception {
        byte[] decodedBytes = Base64.decodeBase64(loadFileAsBytesArray(sourceFile));
        writeByteArraysToFile(targetFile, decodedBytes);
    }

    public static byte[] loadFileAsBytesArray(String fileName) throws Exception {
        File file = new File(fileName);
        int length = (int) file.length();
        BufferedInputStream reader = new BufferedInputStream(new FileInputStream(file));
        byte[] bytes = new byte[length];
        reader.read(bytes, 0, length);
        reader.close();
        return bytes;
    }

    public static void writeByteArraysToFile(String fileName, byte[] content) throws IOException {
        File file = new File(fileName);
        BufferedOutputStream writer = new BufferedOutputStream(new FileOutputStream(file));
        writer.write(content);
        writer.flush();
        writer.close();
    }

    public static String encryptData(String plainText) {
        StandardPBEStringEncryptor textEncryptor = new StandardPBEStringEncryptor();
        textEncryptor.setPassword("20181018");
        return textEncryptor.encrypt(plainText);
    }

    public static String decryptData(String generateText) {
        StandardPBEStringEncryptor textEncryptor = new StandardPBEStringEncryptor();
        textEncryptor.setPassword("20181018");
        return textEncryptor.decrypt(generateText);
    }
}
