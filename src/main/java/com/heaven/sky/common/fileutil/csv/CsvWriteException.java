package com.heaven.sky.common.fileutil.csv;

public class CsvWriteException extends RuntimeException {

    public CsvWriteException(String msg, Exception e) {
        super(msg, e);
    }

}
