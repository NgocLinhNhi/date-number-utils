package com.heaven.sky.common.fileutil.csv;

import com.heaven.sky.common.io.NFiles;
import lombok.Setter;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collection;

import static com.heaven.sky.common.constant.IConstants.UTF8;

public class SimpleCsvPrinter implements CsvPrinter, Closeable {
    //
    @Setter
    protected boolean autoFlush;
    private final CSVPrinter writer;

    private static final byte[] BOM = {(byte) 0xEF, (byte) 0xBB, (byte) 0xBF};

    public SimpleCsvPrinter(String fileName) {
        this(fileName, false);
    }

    public SimpleCsvPrinter(String fileName, boolean append) {
        try {
            File file = NFiles.newFile(fileName);
            this.writer = new CSVPrinter(new FileWriter(file, append), CSVFormat.DEFAULT);
        } catch (Exception e) {
            throw new IllegalStateException("can't create csv printer", e);
        }
    }

    public SimpleCsvPrinter(Appendable appendable) {
        try {
            this.writer = new CSVPrinter(appendable, CSVFormat.DEFAULT);
        } catch (Exception e) {
            throw new IllegalStateException("can't create csv printer", e);
        }
    }

    public SimpleCsvPrinter(OutputStream stream) {
        this(stream, StandardCharsets.UTF_8);
    }

    public SimpleCsvPrinter(OutputStream stream, Charset charset) {
        this(new OutputStreamWriter(stream, charset));
    }

    @Override
    public void flush() {
        try {
            this.writer.flush();
        } catch (Exception e) {
            throw new CsvWriteException("flush data error", e);
        }
    }

    @Override
    public void close() {
        try {
            this.writer.close();
        } catch (Exception e) {
            throw new CsvWriteException("close csv writer error", e);
        }
    }

    @Override
    public void write(String line) {
        try {
            this.writer.printRecord(line.split(","));
        } catch (Exception e) {
            throw new CsvWriteException("write line: " + line + " error", e);
        }
        this.postWrite();
    }

    @Override
    public void write(String... values) {
        try {
            this.writer.printRecord(values);
        } catch (Exception e) {
            throw new CsvWriteException("write line array error", e);
        }
        this.postWrite();

    }

    @Override
    public void write(Iterable<String> values) {
        try {
            this.writer.printRecord(values);
        } catch (Exception e) {
            throw new CsvWriteException("write line iterable error", e);
        }
        this.postWrite();

    }

    @Override
    public void write(Collection<String[]> lines) {
        try {
            for (String[] arr : lines)
                this.writer.printRecord(arr);
        } catch (Exception e) {
            throw new CsvWriteException("write line collection error", e);
        }
        this.postWrite();
    }

    @Override
    public void writeLines(Collection<String> lines) {
        try {
            for (String line : lines)
                this.writer.printRecord(line.split(","));
        } catch (Exception e) {
            throw new CsvWriteException("write lines collection error", e);
        }
        this.postWrite();
    }

    protected void postWrite() {
        if (autoFlush)
            flush();
    }

    public SimpleCsvPrinter(Appendable appendable, CSVFormat format) {
        try {
            this.writer = new CSVPrinter(appendable, format);
        } catch (Exception e) {
            throw new IllegalStateException("can't create csv printer", e);
        }
    }

    /**
     * write file by charset
     *
     * @param csvFileName
     * @param charset
     * @param csvFormat
     * @return
     * @throws Exception
     */
    public static SimpleCsvPrinter writeFileCsv(
            String folderName,
            String csvFileName,
            String charset,
            CSVFormat csvFormat) throws Exception {
        checkCreateFolder(folderName);
        OutputStream out = new FileOutputStream(csvFileName, false);
        OutputStreamWriter writerStream = new OutputStreamWriter(out, Charset.forName(charset));
        return new SimpleCsvPrinter(writerStream, csvFormat);
    }

    /**
     * write file with BOM ( 3byte at the first of file = BOM)
     *
     * @param csvFileName
     * @param charset
     * @return
     * @throws Exception
     */
    public static SimpleCsvPrinter writeFileCsv(
            String folderName,
            String csvFileName,
            String charset) throws Exception {
        checkCreateFolder(folderName);
        OutputStream out = new FileOutputStream(csvFileName, false);
        out.write(BOM);
        OutputStreamWriter writerStream = new OutputStreamWriter(out, Charset.forName(charset));
        SimpleCsvPrinter printer = new SimpleCsvPrinter(writerStream, CSVFormat.INFORMIX_UNLOAD_CSV);
        return printer;
    }

    public static void checkCreateFolder(String folderName) {
        File file = new File(folderName);
        if (!file.exists()) {
            file.mkdirs();
        }
    }


    public static void main(String[] args) throws Exception {
        String folderName = "e://test";
        String fileName = "e://test/test.csv";
        SimpleCsvPrinter simpleCsvPrinter = writeFileCsv(folderName, fileName, UTF8, CSVFormat.DEFAULT);
    }


}
