package com.heaven.sky.common.fileutil;

import com.heaven.sky.common.utils.StringUtil;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class FileUtils {
    public static final String UNDER_LINE = "_";
    private static Logger logger = LoggerFactory.getLogger(FileUtils.class);

    /**
     * move file of current folder to another folder -> ứng dụng sao khi resend mailservice move file đến folder khác
     *
     * @param flCurrent
     * @param flDestination
     * @throws IOException
     */
    public static void moveFile(File flCurrent, File flDestination) throws IOException {
        if (flCurrent.exists()) {
            if (flDestination.exists())
                renameToTemporaryName(flDestination, "old");
            File flFolder;
            flFolder = flDestination.getParentFile();
            if ((flFolder != null) && (!flFolder.exists())) {
                if (!flFolder.mkdirs()) {
                    if (!flFolder.exists())
                        throw new IOException("Cannot create directory " + flFolder);
                }
            }
            if (!flCurrent.renameTo(flDestination)) {
                copyFile(flCurrent, flDestination);

                if (!flCurrent.delete()) {
                    if (!flDestination.delete()) {
                    }
                    throw new IOException("Cannot delete already copied file " + flCurrent);
                }
            }
        }
    }

    //Copy nội dung 1 file sang 1 file khác
    private static void copyFile(File flCurrent, File flDestination) throws IOException {
        File flFolder;

        flFolder = flDestination.getParentFile();
        if ((flFolder != null) && (!flFolder.exists())) {
            if (!flFolder.mkdirs()) {
                if (!flFolder.exists())
                    throw new IOException("Cannot create directory " + flFolder);
            }
        }

        try (FileInputStream finInput = new FileInputStream(flCurrent)) {
            FileUtils.copyStreamToFile(finInput, flDestination);
        }

    }

    private static void renameToTemporaryName(File flFileToRename, String strPrefix) throws IOException {
        assert strPrefix != null : "Prefix cannot be null.";

        String strParent;
        StringBuilder sbBuffer = new StringBuilder();
        File flTemp;
        int iIndex = 0;

        strParent = flFileToRename.getParent();

        do {
            iIndex++;
            sbBuffer.delete(0, sbBuffer.length());
            if (strParent != null) {
                sbBuffer.append(strParent);
                sbBuffer.append(File.separatorChar);
            }

            sbBuffer.append(strPrefix);
            sbBuffer.append(UNDER_LINE);
            sbBuffer.append(iIndex);
            sbBuffer.append(UNDER_LINE);
            sbBuffer.append(flFileToRename.getName());

            flTemp = new File(sbBuffer.toString());
        }
        while (flTemp.exists());

        if (!flFileToRename.renameTo(flTemp)) {
            throw new IOException("Cannot rename " + flFileToRename.getAbsolutePath() + " to " + flTemp.getAbsolutePath());
        }
    }

    //Copy nội dung 1 file sang 1 file khác
    private static void copyStreamToFile(InputStream input, File output) throws IOException {
        try (FileOutputStream fOutput = new FileOutputStream(output)) {
            copyStreamToStream(input, fOutput);
        }
    }

    //Copy nội dung 1 file sang 1 file khác
    private static void copyStreamToStream(InputStream input, OutputStream output) throws IOException {
        InputStream is = null;
        OutputStream os = null;
        int ch;

        try {
            if (input instanceof BufferedInputStream) {
                is = input;
            } else {
                is = new BufferedInputStream(input);
            }
            if (output instanceof BufferedOutputStream) {
                os = output;
            } else {
                os = new BufferedOutputStream(output);
            }

            while ((ch = is.read()) != -1) {
                os.write(ch);
            }
            os.flush();
        } finally {
            IOException exec1 = null;
            IOException exec2 = null;
            try {
                if (os != null) {
                    try {
                        os.close();
                    } catch (IOException exec) {
                        exec1 = exec;
                    }
                }
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException exec) {
                        exec2 = exec;
                    }
                }
                if ((exec1 != null) && (exec2 != null)) {
                    throw exec1;
                } else if (exec1 != null) {
                    throw exec1;
                } else if (exec2 != null) {
                    throw exec2;
                }
            }

        }
    }
    //move file --> END


    /**
     * move file đến 1 folder khác nhưng không xóa file cũ ở folder cũ
     *
     * @param file
     * @param filePath
     * @param directory
     * @throws Exception
     */
    public static void moveFileDocs(MultipartFile file, String filePath, String directory) throws Exception {
        File dir = new File(directory);
        if (!dir.exists()) dir.mkdirs();

        if (file != null && filePath != null) {
            File dest = new File(filePath);
            FileCopyUtils.copy(file.getBytes(), dest);
        }
    }

    /**
     * @param path
     * @param s
     */
    public static void write(String path, String s) {
        if (StringUtil.isEmpty(s)) return;
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(path, true);
            fos.write(s.getBytes());
        } catch (Exception e) {
            logger.error("Can not write file", e);
        } finally {
            close(fos);
        }
    }

    /**
     * Download file Util
     *
     * @param response
     * @param link
     */
    public static void downloadSingleFile(HttpServletResponse response, String link) {
        if (StringUtil.isEmpty(link)) return;
        OutputStream out = null;
        FileInputStream in = null;
        try {
            byte[] buffer = new byte[4096];
            String fileName = FilenameUtils.getName(link);
            response.setContentType("application/pdf");
            response.setHeader("Content-Disposition", "attachment; filename=" + URLEncoder.encode(fileName, "UTF-8"));
            out = response.getOutputStream();
            in = new FileInputStream(link);
            int length;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
        } catch (IOException e) {
            logger.info("failed to download file {}", e, link); // erased and fix customer id
        } finally {
            close(in);
            flush(out);
            close(out);
        }
    }

    /**
     * Download file Util
     *
     * @param fileName
     * @param in
     * @param response
     */
    public static void download(String fileName, InputStream in, HttpServletResponse response) throws Exception {
        ServletOutputStream out = null;
        try {
            response.setContentType("application/octet-stream");
            response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
            out = response.getOutputStream();

            byte[] outputByte = new byte[4096];

            int bytesRead;
            while ((bytesRead = in.read(outputByte)) != -1) {
                out.write(outputByte, 0, bytesRead);
            }
        } finally {
            in.close();
            if (out != null) out.flush();
            if (out != null) out.close();
        }
    }

    /**
     *
     * Zip file
     *
     * @param lstLinkFile
     * @return
     * @throws IOException
     */
    public static InputStream createZipStream(List<String> lstLinkFile) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(bos);
        try {
            FileInputStream is = null;
            BufferedInputStream entryStream = null;
            byte data[] = new byte[2048];
            for (String filePath : lstLinkFile) {
                try {
                    Path path = Paths.get(filePath);
                    is = new FileInputStream(filePath);
                    entryStream = new BufferedInputStream(is, 2048);
                    zos.putNextEntry(new ZipEntry(path.getFileName().toString()));
                    int count;
                    while ((count = entryStream.read(data, 0, 2048)) != -1) {
                        zos.write(data, 0, count);
                    }
                } finally {
                    if (entryStream != null) entryStream.close();
                    if (is != null) is.close();
                }

            }
            zos.closeEntry();
            zos.finish();
            if (is != null) is.close();
            bos.close();
            return new ByteArrayInputStream(bos.toByteArray());
        } finally {
            zos.closeEntry();
            zos.close();
            bos.close();
        }
    }

    private static void close(Closeable c) {
        if (c == null)
            return;
        try {
            c.close();
        } catch (IOException ex) {
            logger.info("failed to close", ex); // erased and fix customer id
        }
    }

    private static void flush(Flushable f) {
        if (f == null)
            return;
        try {
            f.flush();
        } catch (IOException ex) {
            logger.info("failed to flush", ex); // erased and fix customer id
        }
    }
}