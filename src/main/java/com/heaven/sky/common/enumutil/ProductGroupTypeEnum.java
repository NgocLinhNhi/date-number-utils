package com.heaven.sky.common.enumutil;

import lombok.Getter;

public enum ProductGroupTypeEnum {

    CHOICE6(1, "type.choice_6"),
    HIGH_LOW(2, "type.high_Low"),
    RANG_IN(4, "type.range_in"),
    RANG_OUT(5, "type.range_out"),
    LADDER(6, "type.ladder");

    @Getter
    private final int intValue;
    @Getter
    private final String texTkey;

    ProductGroupTypeEnum(int intValue, String textKey) {
        this.intValue = intValue;
        this.texTkey = textKey;
    }

    public static ProductGroupTypeEnum getInstanceByIntValue(int value) {
        for (ProductGroupTypeEnum e : ProductGroupTypeEnum.values()) {
            if (value == e.getIntValue())
                return e;
        }
        return null;
    }

    public static void main(String[] args) {
        ProductGroupTypeEnum instanceByIntValue = getInstanceByIntValue(1);
        System.out.println(instanceByIntValue.getTexTkey());
        System.out.println(instanceByIntValue.getIntValue());
    }

}
